#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <stdio.h>
#include <stdlib.h>

//               0   1    2    3   4   5    6    7    8    9  . 
int codes[] = {222, 10, 230, 110, 58, 124, 252, 14, 254, 126, 1};
volatile int flag = 0;
volatile int counter;

void transfer(int num) {
        SPDR = num;
        while (!(SPSR&(1<<SPIF)));
        PORTB |= (1<<PORTB2);
        PORTB &= ~(1<<PORTB2);
}

void segment_number (int num) {
        transfer(codes[num]);
}

ISR(INT0_vect) {
	counter = (counter + 1) % 10;
	flag = 1;
}

ISR(INT1_vect) {
	counter = 0;
	flag = 1;
}

void setup() {
	DDRB |= ((1<<PORTB2)|(1<<PORTB3)|(1<<PORTB5));
	PORTB &= ~((1<<PORTB2)|(1<<PORTB3)|(1<<PORTB5));
	SPCR = ((1<<SPE)|(1<<MSTR));
	SPDR = 0b00000000;
	while (!(SPSR&(1<<SPIF)));
	PORTB |= (1<<PORTB2);
	PORTB &= ~(1<<PORTB2);

        counter = eeprom_read_byte(0x00);
        segment_number(counter);

        DDRD  = 0b00000000;
        PORTD = 0b00001100;

        EICRA |= (1<<ISC01);
        EICRA |= (1<<ISC11);
        EIMSK |= (1<<INT0);
        EIMSK |= (1<<INT1);

        sei();
}

int main() { 
	setup();
       	while (1) {
		if (flag == 1) {
			segment_number(counter);
			eeprom_write_byte(0x00, counter);
			flag = 0;
		}
	} 
	return 0;
}

