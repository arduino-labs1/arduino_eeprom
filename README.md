# Arduino EEPROM
Output number of clicks on the button and save it to EEPROM.

Green button: counter += 1
Red button: counter = 0

## Content
* Arduino Nano
* Shift Register SN74HC595N
* Seven-segment display
* Button
* Resistor 220ω (x8) 
* Connecting wires
* Capacitor 50nf to combat the rattling (x2)

## Run
```bash
$ avr-gcc -g -O1 -mmcu=atmega328p -o main.elf main.c
$ avrdude -v -patmega328p -c arduino -P /dev/ttyUSB0 -U flash:w:'main.elf'
```

